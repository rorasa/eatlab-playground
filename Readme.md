# EALTAB's Challenges

## Data Science Challenges

Data Science challenges involve participating on [Kaggle](https://www.kaggle.com/), the community website for data science project.

1. Register an account on [Kaggle.com](https://www.kaggle.com/)
2. Attempt the given competitions below.
3. Performance is evaluated based on the results, the methods, and also the ranks obtained on Kaggle website.

Current competitions:

- [Titanic: Machine Learning from Disaster](https://www.kaggle.com/c/titanic/data)
- [Predict Future Sales](https://www.kaggle.com/c/competitive-data-science-predict-future-sales)
- [TalkingData AdTracking Fraud Detection Challenge](https://www.kaggle.com/c/talkingdata-adtracking-fraud-detection)

## Programmer Challenges

### Task 1

Using your programming language of choice, develop a software to decode the given
encoded list into a readable text.

The encoded list has the following format and meaning:

- new line: 0xAA 0xAA ; print a linefeed character
- insert a new character: 0xBB 0xBB ; new character added is similar to the previous character
- increase ascii by <length>: 0xCC <size> ; increase the value of the last character by <length>
- decrease ascii by <length>: 0xDD <size> ; decrease the value of the last character by <length>
- print character: 0xEE 0xEE ; print the last character to screen
- end: 0xFF 0xFF ; end of the string

Example:

**Plain text**
```
EATLAB
```

**Encoded list**
```
187,187
204,69
238,238
187,187
221,4
238,238
187,187
204,19
238,238
187,187
221,8
238,238
187,187
221,11
238,238
187,187
204,1
238,238
170,170
255,255
170,170
187,187
187,187
170,170
221,106
238,238
170,170
255,255
```

**Plain text**

[demo1.txt](https://gitlab.com/rorasa/eatlab-playground/raw/master/demo1.txt)

**Encoded list**

[encoded1.txt](https://gitlab.com/rorasa/eatlab-playground/raw/master/encoded_demo1.txt)

### Task 2

Using program developed in task 1, complete the task given in the following file.

[task2.txt](https://gitlab.com/rorasa/eatlab-playground/raw/master/encoded_task2.txt)
