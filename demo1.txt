EATLAB is a deep tech spin-out that helps food and beverage companies capture true customer demands, using Artificial Intelligence and big data.
This is a culmination of the founder's extended research since her PhD at MIT.
We are constantly aggregating behavioral data of consumers in Thailand, ensuring that accuracy of our sales forecast remain high, currently at 70%.
